package diffsquares

// Return the square of the sum of the first n integers
// https://en.wikipedia.org/wiki/Triangular_number
func SquareOfSum(n int) int {
	// The sum of n numbers formula is sum = n(n + 1)/2
	// Benchmark: 0.3183 ns/op
	sum := (n * (n + 1)) >> 1
	return sum * sum
}

// Return the n-th "square pyramidal number" (see
// http://en.wikipedia.org/wiki/Square_pyramidal_number).
func SumOfSquares(n int) int {
	// The sum of k^2 for k = 1 to n is
	//   n(n+1)(2n+1)   (2n^3 + 3n^2 + n)
	//   ------------ = -----------------
	//        6                6
	// Benchmark: 0.3133 ns/op
	return n * (n + 1) * (2*n + 1) / 6
}

// Return the difference between sum of the squares and square of the sums
func Difference(n int) int {
	return SquareOfSum(n) - SumOfSquares(n)
}
