// Package weather return forecast for locations.
package weather

// CurrentCondition: waether condition.
var CurrentCondition string
// CurrentLocation: location e.g. city.
var CurrentLocation string

// Forecast: Return weather condition for location.
func Forecast(city, condition string) string {
	CurrentLocation, CurrentCondition = city, condition
	return CurrentLocation + " - current weather condition: " + CurrentCondition
}
