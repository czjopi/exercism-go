package letter

import (
	"sync"
)

// FreqMap records the frequency of each rune in a given text.
type FreqMap map[rune]int

// Frequency counts the frequency of each rune in a given text and returns this
// data as a FreqMap.
func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

// ConcurrentFrequency counts the frequency of each rune in the given strings,
// by making use of concurrency.
//
//BenchmarkSequentialFrequency
//BenchmarkSequentialFrequency-8   	    5192	    229629 ns/op
//BenchmarkConcurrentFrequency
//BenchmarkConcurrentFrequency-8   	     807	   1496008 ns/op
func ConcurrentFrequency(l []string) FreqMap {
	var sm sync.Mutex
	var wg sync.WaitGroup
	m := FreqMap{}

	for _, s := range l {
		wg.Add(1)

		go func(str string) {
			defer wg.Done()
			for _, v := range str {
				sm.Lock()
				m[v]++
				sm.Unlock()
			}
		}(s)
	}

	wg.Wait()
	return m
}
