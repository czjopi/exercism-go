package hamming

import (
	"errors"
)

// Distance calculate the Hamming Distance between two DNA strands
func Distance(a, b string) (int, error) {
	as := []byte(a)
	bs := []byte(b)
	if len(as) != len(bs) {
		return 0, errors.New("different strands lenght")
	}
	var count int
	for i := 0; i < len(as); i++ {
		if as[i] != bs[i] {
			count++
		}
	}
	return count, nil
}
