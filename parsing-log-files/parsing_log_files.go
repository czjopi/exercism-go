package parsinglogfiles

import (
	"fmt"
	"regexp"
)

func IsValidLine(text string) bool {
	re := regexp.MustCompile(`^\[(?:TRC|DBG|INF|WRN|ERR|FTL)\]`)
	return re.MatchString(text)
}

func SplitLogLine(text string) []string {
	re := regexp.MustCompile(`<(?:\*|~|=|-)*>`)
	return re.Split(text, -1)
}

func CountQuotedPasswords(lines []string) int {
	var count int
	re := regexp.MustCompile(`".*(?i:password).*"`)
	for _, l := range lines {
		if re.MatchString(l) {
			count++
		}
	}
	return count
}

func RemoveEndOfLineText(text string) string {
	re, err := regexp.Compile(`end-of-line[[:digit:]]+`)
	if err != nil {
		fmt.Println(err)
	}
	return re.ReplaceAllString(text, "")
}

func TagWithUserName(lines []string) []string {
	re := regexp.MustCompile(`(User\s+)(\w+)`)
	for ix, l := range lines {
		matches := re.FindStringSubmatch(l)
		if len(matches) > 0 {
			lines[ix] = fmt.Sprintf("%s %s %s", "[USR]", matches[2], l)
		}
	}
	return lines
}
