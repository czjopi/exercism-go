package robotname

import (
	"errors"
	"fmt"
)

// Define the Robot type here.
type Robot struct {
	robotname string
}

// Robot names permutation 26*26*10*10*10=676000
var namesAvailable = make([]string, 0, 676000)

func init() {
	for a := 'A'; a <= 'Z'; a++ {
		for b := 'A'; b <= 'Z'; b++ {
			for i := 0; i <= 999; i++ {
				namesAvailable = append(namesAvailable, fmt.Sprintf("%s%s%03d", string(a), string(b), i))
			}
		}
	}
}

func (r *Robot) Name() (string, error) {
	if n := r.robotname; n == "" {
		if len(namesAvailable) == 0 {
			return "", errors.New("no more names available")
		}
		r.robotname = namesAvailable[0]
		namesAvailable = namesAvailable[1:]
	}
	return r.robotname, nil
}

func (r *Robot) Reset() {
	r.robotname = ""
}
