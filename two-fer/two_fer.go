// Package twofer implements Two-fer or 2-fer message
// 2-fer is short for two for one. One for you and one for me.
package twofer

import "fmt"

// ShareWith return two-fer message
func ShareWith(name string) string {
	if name == "" {
		name = "you"
	}
	return fmt.Sprintf("One for %s, one for me.", name)
}
