package clock

import "fmt"

//BenchmarkAddMinutes
//BenchmarkAddMinutes-8        	322407560	         3.465 ns/op
//BenchmarkSubtractMinutes
//BenchmarkSubtractMinutes-8   	348274468	         3.442 ns/op
//BenchmarkCreateClocks
//BenchmarkCreateClocks-8      	166669020	         7.202 ns/op

// Minutes in day
const minInDay = 1440

// Define the Clock type here.
type Clock struct {
	hours   int
	minutes int
}

// New create new Clock
func New(h, m int) Clock {
	// h*60+m sum of minutes
	// - cut day overlap
	// - add day if negative
	// - cut extra day
	min := ((h*60+m)%minInDay + minInDay) % minInDay
	return Clock{min / 60, min % 60}
}

// Add minutes to Clock
func (c Clock) Add(m int) Clock {
	return New(c.hours, c.minutes+m)
}

// Substract minutes from Clock
func (c Clock) Subtract(m int) Clock {
	return New(c.hours, c.minutes-m)
}

// String represeantion of Clock
func (c Clock) String() string {
	return fmt.Sprintf("%02d:%02d", c.hours, c.minutes)
}
