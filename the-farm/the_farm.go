package thefarm

import (
	"errors"
	"fmt"
)

// See types.go for the types defined for this exercise.

// TODO: Define the SillyNephewError type here.
type MyCustomError struct {
	cows int
}

func (e *MyCustomError) Error() string {
	return fmt.Sprintf("silly nephew, there cannot be %d cows", e.cows)
}

var ScaleError = errors.New("negative fodder")

// DivideFood computes the fodder amount per cow for the given cows.
func DivideFood(weightFodder WeightFodder, cows int) (float64, error) {
	fodder, err := weightFodder.FodderAmount()
	if cows < 0 {
		return 0, &MyCustomError{cows}
	} else if cows == 0 {
		return 0, errors.New("division by zero")
	}
	if err != nil {
		if err == ErrScaleMalfunction && fodder > 0 {
			fodder += fodder
		} else if err == ErrScaleMalfunction && fodder < 0 {
			return 0, ScaleError
		} else {
			return 0, err
		}
	} else if fodder < 0 {
		return 0, ScaleError
	}
	return fodder / float64(cows), nil
}
