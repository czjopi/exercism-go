package tournament

import (
	"errors"
	"fmt"
	"io"
	"log"
	"sort"
	"strings"
)

var IncompleteRecordError = errors.New("incomplete record")

type Score struct {
	MP int
	W  int
	D  int
	L  int
	P  int
}

type ScoreBoard map[string]*Score

// For ScoreBoard map sort
type ScorePair struct {
	Key   string
	Value *Score
}

type PairList []ScorePair

func (s PairList) Len() int {
	return len(s)
}

func (s PairList) Less(i, j int) bool {
	if s[i].Value.P == s[j].Value.P {
		return s[i].Key < s[j].Key
	}
	return s[i].Value.P > s[j].Value.P
}

func (s PairList) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

func Tally(reader io.Reader, writer io.Writer) error {
	var err error
	scoreBoard := make(ScoreBoard)

	// Read all input
	rec, err := io.ReadAll(reader)
	if err != nil {
		log.Fatal(err)
	}

	// Split input to lines
	lines := strings.Split(string(rec), "\n")
	for _, line := range lines {
		if len(line) == 0 || line[0] == '#' {
			continue
		}

		record := strings.Split(string(line), ";")
		if len(record) != 3 {
			return IncompleteRecordError
		}
		result := record[2]

		// create record for first team if not exists
		if _, ok := scoreBoard[record[0]]; !ok {
			scoreBoard[record[0]] = &Score{}
		}

		// create record for second team if not exists
		if _, ok := scoreBoard[record[1]]; !ok {
			scoreBoard[record[1]] = &Score{}
		}

		team1 := scoreBoard[record[0]]
		team2 := scoreBoard[record[1]]

		switch result {
		case "win":
			team1.W++
			team1.P += 3
			team2.L++
		case "loss":
			team1.L++
			team2.P += 3
			team2.W++
		case "draw":
			team1.D++
			team1.P++
			team2.D++
			team2.P++
		default: // only win, loss, draw allowed
			return IncompleteRecordError
		}
		team1.MP++
		team2.MP++
	}

	// make PairList to sort scoreBoard
	s := make(PairList, len(scoreBoard))

	i := 0
	for k, v := range scoreBoard {
		s[i] = ScorePair{k, v}
		i++
	}

	// Sort PairList
	sort.Sort(s)

	// Write sorted PairList to writer
	scorePrint(writer, s)
	return err
}

func scorePrint(writer io.Writer, p PairList) {
	fmt.Fprintf(writer, "%-30s | %2s | %2s | %2s | %2s | %2s\n", "Team", "MP", "W", "D", "L", "P")

	format := "%-30s | %2d | %2d | %2d | %2d | %2d\n"
	for i := 0; i < len(p); i++ {
		fmt.Fprintf(
			writer,
			format,
			p[i].Key,
			p[i].Value.MP,
			p[i].Value.W,
			p[i].Value.D,
			p[i].Value.L,
			p[i].Value.P,
		)
	}
}
