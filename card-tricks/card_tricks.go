package cards

func isIndexValid(slice []int, index int) bool {
	return index >= 0 && index < len(slice)
}

// FavoriteCards return favorite cards.
func FavoriteCards() []int {
	return []int{2, 6, 9}
}

// PrependItems prepend slice with values.
func PrependItems(slice []int, values ...int) []int {
	return append(values, slice...)
}

// GetItem retrieves an item from a slice at given position.
// If item is not in a slice returns -1.
func GetItem(slice []int, index int) int {
	if isIndexValid(slice, index) {
		return slice[index]
	}
	return -1
}

// SetItem writes an item to a slice at given position overwriting an existing value.
// If the index is out of range the value needs to be appended.
func SetItem(slice []int, index, value int) []int {
	if isIndexValid(slice, index) {
		slice[index] = value
	} else {
		slice = append(slice, value)
	}
	return slice
}

// PrefilledSlice creates a slice of given length and prefills it with the given value.
func PrefilledSlice(value, length int) []int {
	s := []int{}
	for i := 0; i < length; i++ {
		s = append(s, value)
	}
	return s
}

// RemoveItem removes an item from a slice by modifying the existing slice.
func RemoveItem(slice []int, index int) []int {
	if isIndexValid(slice, index) {
		slice = append(slice[:index], slice[index+1:]...)
	}
	return slice
}
