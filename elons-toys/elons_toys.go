package elon

import "fmt"

// Drive method - drive track once
func (c *Car) Drive() {
	if c.battery > 0 && c.battery-c.batteryDrain >= 0 {
		c.battery -= c.batteryDrain
		c.distance += c.speed
	}
}

// DisplayDistance - display driven distance
func (c Car) DisplayDistance() string {
	return fmt.Sprintf("Driven %d meters", c.distance)
}

// DisplayBattery - display the battery percentage
func (c Car) DisplayBattery() string {
	return fmt.Sprintf("Battery at %d%%", c.battery)
}

// CanFinish - can car finish a race
func (c Car) CanFinish(trackDistance int) bool {
	return c.battery/c.batteryDrain*c.speed >= trackDistance
}
