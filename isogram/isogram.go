package isogram

import "strings"

func IsIsogram(word string) bool {
	if len(word) == 0 {
		return true
	}
	chars := map[string]int{}
	for _, v := range word {
		s := strings.ToUpper(string(v))
		if _, ok := chars[s]; ok {
			if s != " " && s != "-" {
				return false
			}
		}
		chars[s] = 1
	}
	return true
}
