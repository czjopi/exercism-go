package grains

import (
	"errors"
)

// Square return number of grains on chessboard square
//
// xn = ar**(n-1)
// a=1 - the first term
// r=2 - the "common ratio" between terms is a doubling
func Square(number int) (uint64, error) {
	var sum uint64
	var err error
	if number <= 0 || number > 64 {
		err = errors.New("square out of chessboard")
	}
	// Benchmark: 216.5 ns/op
	// sum = uint64(1 * math.Pow(2, float64(number-1)))
	// Benchmark: 24.46 ns/op
	sum = uint64(1 << uint(number-1))
	return sum, err
}

// Total return total number of grans on chessboard
//
// sum of series = a(1 – r**n)/(1 – r).
// a=1 - the first term
// r=2 - the "common ratio" between terms is a doubling
// n - is the number of terms (64)
func Total() uint64 {
	// Benchmark: 17.13 ns/op
	// return uint64(1 * (1 - math.Pow(2, float64(64))) / (1 - 2))
	// Benchmark: 0.3132 ns/op
	return uint64((1 << 64) - 1)
}
