package gross

// Units stores the Gross Store unit measurements.
func Units() map[string]int {
	units := map[string]int{
		"quarter_of_a_dozen": 3,
		"half_of_a_dozen":    6,
		"dozen":              12,
		"small_gross":        120,
		"gross":              144,
		"great_gross":        1728,
	}
	return units
}

// NewBill creates a new bill.
func NewBill() map[string]int {
	return make(map[string]int)
}

// AddItem adds an item to customer bill.
func AddItem(bill, units map[string]int, item, unit string) bool {
	if _, present := units[unit]; present {
		if _, prs := bill[item]; prs {
			bill[item] += units[unit]
		} else {
			bill[item] = units[unit]
		}
		return true
	} else {
		return false
	}
}

// RemoveItem removes an item from customer bill.
func RemoveItem(bill, units map[string]int, item, unit string) bool {
	if _, present := units[unit]; present {
		if _, prs := bill[item]; prs {
			if a := bill[item] - units[unit]; a < 0 {
				return false
			} else if a == 0 {
				delete(bill, item)
				return true
			} else {
				bill[item] = a
				return true
			}
		} else {
			return false
		}
		return true
	} else {
		return false
	}
}

// GetItem returns the quantity of an item that the customer has in his/her bill.
func GetItem(bill map[string]int, item string) (int, bool) {
	if _, present := bill[item]; present {
		return bill[item], true
	} else {
		return 0, false
	}
}
