package luhn

import (
	"strings"
)

// Valid number
// [Luhn algorithm](https://en.wikipedia.org/wiki/Luhn_algorithm)
func Valid(id string) bool {
	id = strings.Replace(id, " ", "", -1)
	if len(id) < 2 {
		return false
	}
	var sum, c int
	for i := len(id); i > 0; i-- {
		n := int(id[i-1] - '0')
		if !(n >= 0 && n <= 9) {
			return false
		}
		if (c+1)%2 == 0 {
			n *= 2
			if n > 9 {
				n -= 9
			}
		}
		sum += n
		c += 1
	}
	return sum%10 == 0
}
